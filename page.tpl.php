<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts; ?>
</head>

<body class="drupal">
<div id="top">
	<p class="hide">Skip to: <a href="#sitemenu" accesskey="2">Site menu</a> | <a href="#maincontent" accesskey="3">Main content</a></p>
</div>

<div id="container" class="clear-block">

  <div id="main">

    <div id="header" class="clear-block">
      <div id="logo">
        <?php if ($logo): ?>
        <a id="logo-image" href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>" rel="home"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
        <?php endif; ?>

        <?php if ($site_name): ?>
        <h1>[<a href="<?php print $base_path ?>" title="<?php print t('Home') ?>" accesskey="4"><?php print $site_name ?></a>]</h1>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
        <span id="tagline"><?php print $site_slogan ?></span>
        <?php endif; ?>
      </div>

  		<div id="header-region">
        <?php if ($header): ?>
        <?php print $header ?>
        <?php endif; ?>
  		</div>
    </div>

    <div id="content">
      <?php if ($title): ?>
      <h1 class="title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>

      <?php print $messages ?>

      <?php if ($breadcrumb): ?>
      <div class="tabs"><?php print $breadcrumb; ?></div>
      <?php endif; ?>

      <?php if ($tabs): ?>
      <div class="tabs"><?php print $tabs; ?></div>
      <?php endif; ?>

      <?php print $help ?>
      <?php print $content; ?>
    </div>

  </div><!-- #main -->

  <div id="sidebar">
    <h2 id="sitemenu" class="sidelink menuheader">Site menu:</h2>

    <?php if ($primary_links): ?>
    <div id="primary-links"><?php print theme('links', $primary_links); ?></div>
    <?php endif; ?>

    <a class="hide" href="#top" accesskey="1">Top of page</a>

    <?php if ($right): ?>
    <?php print $right ?>
    <?php endif; ?>

    <?php if ($right_note): ?>
    <div id="right-note">
      <?php print $right_note ?>
    </div>
    <?php endif; ?>

  </div><!-- #sidebar -->

</div><!-- #container -->

<div id="footer">
  <?php if ($secondary_links): ?>
  <div><?php print theme('links', $secondary_links); ?></div>
  <?php endif; ?>
  <?php if ($footer_message): ?>
  <div><?php print $footer_message; ?></div>
  <?php endif; ?>
  <div>
    Drupal Theme by <a href="http://www.chrisherberte.com">Chris Herberte</a> | Original design by <a href="http://andreasviklund.com/">Andreas Viklund</a>
  </div>
</div>

<?php print $closure; ?>
</body>
</html>