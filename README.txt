Adopted: 17 May 2007

Fixed-width 2-column layout, built with speed and accessibility in mind. 
It conforms to a WCAG 1.0 Triple-A rating and to Section 508. 

The design should load in less than 3 seconds on a 56k dialup modem. 

Also supported: browser-based font resizing and accesskey navigation. 

The andreas03 template has a small file size, a simple layout for easy editing, 
good accessibility features and semantic markup. Five small images have been
used for the design, adding only 3 kilobytes to the template load size
(which is approximately 12kb).

The images are linked from the stylesheet, which means that they will not show
up in browsers that does not support CSS. They can also be safely removed, since
the images do not affect the layout in any way. 

In CSS-enabled browsers the removed images will be replaced by a background 
color. In browsers with no CSS support, there will be no difference.

